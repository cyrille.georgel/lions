<!--
@Author: Cyrille Georgel <cgeorgel>
@Date:   2016-07-14T19:21:57+01:00
@Email:  cyrille.georgel@gmail.com
@Project: Project
@Last modified by:   cgeorgel
@Last modified time: 2016-09-25T00:38:55+01:00
-->



<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

function randKey($lenght = 24)
{
    /* retuns a simple rand key */

	$randKey = '';

	$chars = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
	'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2',
	'3', '4', '5', '6', '7', '8', '9');

	for($i=0; $i<$lenght; $i++)
	{
		$randKey .= $chars[rand(0, count($chars)-1)];
	}

	return $randKey;
}

# events Dublin Lions Club for year 2016/2017:

$events =
'[
{"dtstart":"2016-09-27 19:00:00","dtend":"2016-09-27 22:00:00","summary":"St Joseph\'s Clonsilla","description":""},
{"dtstart":"2016-10-08 10:00:00","dtend":"2016-10-08 18:00:00","summary":"Diabetes Screening","description":""},
{"dtstart":"2016-10-18 19:00:00","dtend":"2016-10-18 22:00:00","summary":"St Mary\'s","description":""},
{"dtstart":"2016-10-20 20:00:00","dtend":"2016-10-20 23:00:00","summary":"Regular meeting","description":"Radisson Hotel"},
{"dtstart":"2016-10-27 19:30:00","dtend":"2016-10-27 21:30:00","summary":"Bridgend Choir","description":"Pepper Cannister Church"},
{"dtstart":"2016-11-17 20:00:00","dtend":"2016-11-17 23:00:00","summary":"Regular meeting","description":"Radisson Hotel"},
{"dtstart":"2016-11-20 00:00:00","dtend":"2016-11-20 00:00:00","summary":"Annual Charity Walk","description":""},
{"dtstart":"2016-11-25 12:00:00","dtend":"2016-11-25 14:00:00","summary":"Christmas Lunch","description":""},
{"dtstart":"2016-11-25 10:00:00","dtend":"2016-11-25 18:00:00","summary":"Food Appeal","description":""},
{"dtstart":"2016-11-26 10:00:00","dtend":"2016-11-26 18:00:00","summary":"Food Appeal","description":""},
{"dtstart":"2016-11-27 10:00:00","dtend":"2016-11-27 18:00:00","summary":"Food Appeal","description":""},
{"dtstart":"2016-11-26 20:00:00","dtend":"2016-11-26 23:00:00","summary":"Nandri Ball","description":""},
{"dtstart":"2016-12-02 10:00:00","dtend":"2016-12-02 18:00:00","summary":"Food Appeal","description":""},
{"dtstart":"2016-12-03 10:00:00","dtend":"2016-12-03 18:00:00","summary":"Food Appeal","description":""},
{"dtstart":"2016-12-04 10:00:00","dtend":"2016-12-04 18:00:00","summary":"Food Appeal","description":""},
{"dtstart":"2016-12-06 19:00:00","dtend":"2016-12-06 22:00:00","summary":"St Joseph\'s Clonsilla","description":""},
{"dtstart":"2016-12-17 15:00:00","dtend":"2016-12-17 18:00:00","summary":"St Mary\'s","description":""},
{"dtstart":"2017-01-20 00:00:00","dtend":"2017-01-20 00:00:00","summary":"Eric\'s Party","description":"GPO"},
{"dtstart":"2017-01-26 20:00:00","dtend":"2017-01-26 23:00:00","summary":"Regular meeting","description":"Radisson Hotel"},
{"dtstart":"2017-02-16 20:00:00","dtend":"2017-02-16 23:00:00","summary":"Regular meeting","description":"Radisson Hotel"},
{"dtstart":"2017-02-24 12:00:00","dtend":"2017-02-24 14:00:00","summary":"Lions Rugby Lunch","description":""},
{"dtstart":"2017-03-07 19:00:00","dtend":"2017-03-07 22:00:00","summary":"St Joseph\'s Clonsilla","description":""},
{"dtstart":"2017-03-16 20:00:00","dtend":"2017-03-16 23:00:00","summary":"Regular meeting","description":"Radisson Hotel"},
{"dtstart":"2017-03-29 19:00:00","dtend":"2017-03-29 22:00:00","summary":"St Mary\'s","description":""},
{"dtstart":"2017-03-31 20:00:00","dtend":"2017-03-31 23:00:00","summary":"Charter Night","description":""},
{"dtstart":"2017-04-08 10:00:00","dtend":"2017-04-08 18:00:00","summary":"Diabetes Screening","description":""},
{"dtstart":"2017-04-20 20:00:00","dtend":"2017-04-20 23:00:00","summary":"Regular meeting","description":"Radisson Hotel"},
{"dtstart":"2017-05-09 19:00:00","dtend":"2017-05-09 22:00:00","summary":"St Joseph\'s Clonsilla","description":""},
{"dtstart":"2017-05-18 20:00:00","dtend":"2017-05-18 23:00:00","summary":"Regular meeting","description":"Radisson Hotel"},
{"dtstart":"2017-05-19 12:00:00","dtend":"2017-05-19 14:00:00","summary":"Lions Summer Lunch","description":""},
{"dtstart":"2017-05-31 19:00:00","dtend":"2017-05-31 22:00:00","summary":"St Mary\'s","description":""},
{"dtstart":"2017-06-15 20:00:00","dtend":"2017-06-15 23:00:00","summary":"Regular meeting","description":"Radisson Hotel"},
{"dtstart":"2017-06-09 09:00:00","dtend":"2017-06-09 18:00:00","summary":"Trabolgan","description":""},
{"dtstart":"2017-06-10 09:00:00","dtend":"2017-06-10 18:00:00","summary":"Trabolgan","description":""},
{"dtstart":"2017-06-11 09:00:00","dtend":"2017-06-11 18:00:00","summary":"Trabolgan","description":""},
{"dtstart":"2017-06-12 09:00:00","dtend":"2017-06-12 18:00:00","summary":"Trabolgan","description":""},
{"dtstart":"2017-06-13 09:00:00","dtend":"2017-06-13 18:00:00","summary":"Trabolgan","description":""},
{"dtstart":"2017-06-14 09:00:00","dtend":"2017-06-14 18:00:00","summary":"Trabolgan","description":""},
{"dtstart":"2017-06-15 09:00:00","dtend":"2017-06-15 18:00:00","summary":"Trabolgan","description":""},
{"dtstart":"2017-06-16 09:00:00","dtend":"2017-06-16 18:00:00","summary":"Trabolgan","description":""}
]';

$events = json_decode($events);

#print_r($events);


$vCal = '';

$vCal .= 'BEGIN:VCALENDAR' . "\n";
$vCal .= 'VERSION:2.0' . "\n";
$vCal .= 'PRODID:-//BlueRock Cyrille//Dublin Lions//EN' . "\n";

foreach($events as $event)
{
    $dtstamp = date('Ymd') . 'T' . date('His');

    $dtstart = strtotime($event->dtstart);
    $dtstart = date('Ymd', $dtstart) . 'T' . date('His', $dtstart);

    $dtend = strtotime($event->dtend);
    $dtend = date('Ymd', $dtend) . 'T' . date('His', $dtend);

    $uid = $dtstamp . '-'  . 'dublin-lions' . '-'  . randKey(4) . '@cyrille.me';

    $vCal .= 'BEGIN:VEVENT' . "\n";
    $vCal .= 'UID:' . $uid . "\n";
    $vCal .= 'DTSTAMP:' . $dtstamp . "\n";
    $vCal .= 'DTSTART:' . $dtstart . "\n";
    $vCal .= 'DTEND:' . $dtend . "\n";
    $vCal .= 'SUMMARY:'. $event->summary . "\n";
    $vCal .= 'DESCRIPTION:'. $event->description . "\n";
    $vCal .= 'END:VEVENT' . "\n";

}

$vCal .= 'END:VCALENDAR' . "\n";

print $vCal;

?>
